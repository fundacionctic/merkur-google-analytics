/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 13, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import org.junit.Test;

/**
 * Tests the GoogleAnalyticsAPI basic functionality.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsAPITest {
    
    private GoogleAnalyticsAPI googleAnalyticsAPI = 
            GoogleAnalyticsAPI.newInstance(
            GOOGLE_ANALYTICS_CONFIG_FILE_PATH, 
            TRACKING_ID);
    
    /**
     * This method tests the generation of the basic JS snippet.
     */
    @Test
    public void testGenerateBasicSnippetAPI() {
        String snippet = googleAnalyticsAPI.generateJS();
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET));
    }
    
    /**
     * This methods tests the generation of a JS snippet with two trackers,
     * one for the application and another one for the CTIC account. 
     * 
     * That way we will have access to the analytics from two accounts.
     */
    @Test
    public void testCticTrakerSnippetAPI() {
        String snippet = 
                googleAnalyticsAPI.cticTrakingId(CTIC_TRACKING_ID).generateJS();
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_CTIC));
    }
     
     /**
      * This method tests the generation of a JS snippet with a 
      * send pageview command.
      */
    @Test
    public void testSendAPI() {
        String snippet = 
                googleAnalyticsAPI
                .cticTrakingId(CTIC_TRACKING_ID)
                .send(PAGEVIEW)
                .generateJS();
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_PAGEVIEW));
    }
    
    /**
     * This method tests the generation of a snippet with an event command.
     */
    @Test
    public void testEventAPI() {
        GoogleAnalyticsEvent event = GoogleAnalyticsEvent
                .newInstance(CATEGORY, ACTION);
        String snippet = 
                googleAnalyticsAPI
                .cticTrakingId(CTIC_TRACKING_ID)
                .send(PAGEVIEW)
                .event(event)
                .generateJS();
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_EVENT));
    }
    
    private static final String GOOGLE_ANALYTICS_CONFIG_FILE_PATH = 
            "src/test/resources/googleAnalyticsConfiguration.properties";
    private static final String TRACKING_ID = "UA-48015716-1";
    private static final String CTIC_TRACKING_ID = "UA-48015717-1";
    private static final String PAGEVIEW = "pageview";
    private static final String CATEGORY = "button";
    private static final String ACTION = "click";
    private static final String EXPECTED_SNIPPET = 
        "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
        "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
        "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
        "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
        "ga('create', '" + TRACKING_ID + "', 'auto');\n";
    private static final String EXPECTED_SNIPPET_CTIC = 
       EXPECTED_SNIPPET + 
       "ga('create', '" + CTIC_TRACKING_ID + "', 'auto', {'name': 'cticTracker'});\n";
    private static final String EXPECTED_SNIPPET_PAGEVIEW = 
       EXPECTED_SNIPPET_CTIC + 
       "ga('send', '" + PAGEVIEW + "');\n";
    private static final String EXPECTED_SNIPPET_EVENT = 
       EXPECTED_SNIPPET_PAGEVIEW + 
       "ga('send', 'event', '" + CATEGORY + "', '" + ACTION + "');\n";

}
