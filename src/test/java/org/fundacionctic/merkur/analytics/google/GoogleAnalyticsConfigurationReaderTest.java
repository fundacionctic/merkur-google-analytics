/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 12, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import org.junit.Test;

/**
 *
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsConfigurationReaderTest {
    
    private static final String JS_TEMPLATE = 
        "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
        "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
        "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
        "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
        "ga('create', 'UA-XXXX-Y', 'auto');";
    private static final String TRACKING_ID_REGEX = "UA-XXXX-Y";
    private static final String TEST_CONFIG_FILE_PATH = 
            "src/test/resources/googleAnalyticsConfiguration.properties";
    
    private GoogleAnalyticsConfigurationReader configurationReader = 
            new GoogleAnalyticsConfigurationReader();
    private File configurationPropertiesFile = new File(TEST_CONFIG_FILE_PATH);
    
    @Test
    public void testReadConfiguration() throws FileNotFoundException, IOException {
        GoogleAnalyticsConfiguration configuration = 
                configurationReader.readFrom(configurationPropertiesFile);
         assertThat("The configuration reader has NOT read properly the JS template file", 
                configuration.getGoogleAnalyticsJSTemplate(), 
                equalTo(JS_TEMPLATE));
         assertThat("The configuration reader has NOT read properly the tracking ID regex", 
                configuration.getGoogleAnalyticsTrakingIdRegex(), 
                equalTo(TRACKING_ID_REGEX));
    }

}
