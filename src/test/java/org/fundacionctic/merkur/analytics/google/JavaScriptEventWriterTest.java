/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 18, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests how to write properly the JS Google Analytics event code lines.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class JavaScriptEventWriterTest {
    
    private JavaScriptEventWriter eventWriter;
    
    @Before
    public void setUp() {
        GoogleAnalyticsConfiguration configuration = 
                new GoogleAnalyticsConfiguration(JS_TEMPLATE, TRACKING_ID_REGEX);
        configuration.setEventLineTemplate(EVENT_LINE_TEMPLATE);
        configuration.setjQueryEventLineTemplate(EVENT_LINE_JQUERY_TEMPLATE);
        configuration.setCategoryRegex(CATEGORY_REGEX);
        configuration.setActionRegex(ACTION_REGEX);
        configuration.setLabelRegex(LABEL_REGEX);
        configuration.setValueRegex(VALUE_REGEX);
        configuration.setjQueryIdRegex(JQUERY_ID_REGEX);
        configuration.setScrollEventLineTemplate(EVENT_LINE_SCROLL_TEMPLATE);
        eventWriter = new JavaScriptEventWriter(configuration);
    }
    
    /**
     * Tests the code generation whenever all event properties are provided.
     */
    @Test
    public void testCompleteEvent() {
        GoogleAnalyticsEvent event = 
                GoogleAnalyticsEvent
                .newInstance(CATEGORY, ACTION).label(LABEL).value(VALUE).jqueryId(JQUERY_ID);
        googleAnalyticsTracker.addEvent(event);
        String snippet = 
            eventWriter.writeSnippetFor(googleAnalyticsTracker);
         assertThat("The generated google analytics JS EVENT snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_EVENT_COMPLETE));
    }
    
    /**
     * Tests the code generation without jQuery.
     */
    @Test
    public void testEventWithoutJquery() {
        GoogleAnalyticsEvent event = 
                GoogleAnalyticsEvent
                .newInstance(CATEGORY, ACTION).label(LABEL).value(VALUE);
        googleAnalyticsTracker.addEvent(event);
        String snippet = 
            eventWriter.writeSnippetFor(googleAnalyticsTracker);
         assertThat("The generated google analytics JS EVENT snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_EVENT_WITHOUT_JQUERY));
    }
    
    @Test
    public void testEventWithoutValueAndLabel() {
        GoogleAnalyticsEvent event = 
                GoogleAnalyticsEvent
                .newInstance(CATEGORY, ACTION);
        googleAnalyticsTracker.addEvent(event);
        String snippet = 
            eventWriter.writeSnippetFor(googleAnalyticsTracker);
         assertThat("The generated google analytics JS EVENT snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_EVENT_WITHOUT_VALUE_AND_LABEL));
    }
    
    @Test
    public void testScrollEvent() {
        GoogleAnalyticsEvent event = 
                GoogleAnalyticsEvent
                .newInstance(CATEGORY, SCROLL_ACTION);
        googleAnalyticsTracker.addEvent(event);
        String snippet = 
            eventWriter.writeSnippetFor(googleAnalyticsTracker);
         assertThat("The generated google analytics JS EVENT snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_EVENT_SCROLL));
    }
    
    private static final String JS_TEMPLATE = 
        "  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
        "  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
        "  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
        "  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
        "  ga('create', 'UA-XXXX-Y', 'auto');";
    private static final String EVENT_LINE_JQUERY_TEMPLATE = 
        "// Using jQuery Event API v1.3\n" +
        "$('jqueryid').on('action', function() {\n" +
        "  ga('send', 'event', 'category', 'action', 'label', value);\n" +
        "});";
    private static final String EVENT_LINE_SCROLL_TEMPLATE = 
        "// Using jQuery Event API v1.3\n" +
        "$(document).on('scroll', function() {\n" +
        "  $.scrollDepth();\n" +
        "});"; 
    private static final String EVENT_LINE_TEMPLATE = 
        "ga('send', 'event', 'category', 'action', 'label', value);";
    private static final String TRACKING_ID_REGEX = "UA-XXXX-Y";
    private static final String CATEGORY_REGEX = "category";
    private static final String ACTION_REGEX = "action";
    private static final String LABEL_REGEX = "label";
    private static final String VALUE_REGEX = "value";
    private static final String JQUERY_ID_REGEX = "jqueryid";
    private static final String TRACKING_ID = "UA-48015716-1";
    private GoogleAnalyticsTracker googleAnalyticsTracker = 
            new GoogleAnalyticsTracker(TRACKING_ID);
    private static final String EXPECTED_SNIPPET_EVENT_COMPLETE = 
       "// Using jQuery Event API v1.3\n" +
       "$('#button').on('click', function() {\n" +
       "  ga('send', 'event', 'button', 'click', 'nav buttons', 4);\n" +
       "});\n"; 
    private static final String EXPECTED_SNIPPET_EVENT_WITHOUT_JQUERY = 
       "ga('send', 'event', 'button', 'click', 'nav buttons', 4);\n";
    private static final String EXPECTED_SNIPPET_EVENT_WITHOUT_VALUE_AND_LABEL = 
       "ga('send', 'event', 'button', 'click');\n";
    private static final String EXPECTED_SNIPPET_EVENT_SCROLL = 
       "// Using jQuery Event API v1.3\n" +
       "$(document).on('scroll', function() {\n" +
       "  $.scrollDepth();\n" +
       "});\n"; 
    private static final String CATEGORY = "button";
    private static final String ACTION = "click";
    private static final String SCROLL_ACTION = "scroll";
    private static final String LABEL = "nav buttons";
    private static final String VALUE = "4";
    private static final String JQUERY_ID = "#button";
}
