/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 12, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import org.junit.Before;

/**
 *
 * @author josemanuel
 * @version 1.0
 */
public class JavaScriptWriterTest {
    
    @Before
    public void setUp() {
        GoogleAnalyticsConfiguration configuration = 
                new GoogleAnalyticsConfiguration(JS_TEMPLATE, TRACKING_ID_REGEX);
        configuration.setCticTrakerLineTemplate(CTIC_LINE_TEMPLATE);
        configuration.setCticTrakingIdRegex(CTIC_TRACKING_ID_REGEX);
        configuration.setSendLineTemplate(SEND_LINE_TEMPLATE);
        configuration.setHitRegex(HIT_REGEX);
        configuration.setEventLineTemplate(EVENT_LINE_TEMPLATE);
        configuration.setjQueryEventLineTemplate(EVENT_LINE_JQUERY_TEMPLATE);
        configuration.setCategoryRegex(CATEGORY_REGEX);
        configuration.setActionRegex(ACTION_REGEX);
        configuration.setLabelRegex(LABEL_REGEX);
        configuration.setValueRegex(VALUE_REGEX);
        configuration.setjQueryIdRegex(JQUERY_ID_REGEX);
        javaScriptWriter = new JavaScriptWriter(configuration);
    }
    
    @Test
    public void testWriteGoogleAnalyticsSnippet() {
        String snippet = 
                javaScriptWriter.writeSnippetFor(googleAnalyticsTracker);
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET));
    }
    
    @Test
    public void testWriteCticIdSnippet() {
        googleAnalyticsTracker.setCticTrackingId(CTIC_TRACKING_ID);
        String snippet = 
                javaScriptWriter.writeSnippetFor(googleAnalyticsTracker);
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_CTIC));
    }
    
    @Test
    public void testWriteSendSnippet() {
        googleAnalyticsTracker.addHit(PAGEVIEW);
        String snippet = 
                javaScriptWriter.writeSnippetFor(googleAnalyticsTracker);
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_SEND));
    }
    
    @Test
    public void testWriteBasicEventSnippet() {
        GoogleAnalyticsEvent event = 
                GoogleAnalyticsEvent.newInstance(CATEGORY, ACTION);
        googleAnalyticsTracker.addEvent(event);
        String snippet = 
                javaScriptWriter.writeSnippetFor(googleAnalyticsTracker);
        assertThat("The generated google analytics JS snippet is NOT the one expected", 
                snippet, equalTo(EXPECTED_SNIPPET_EVENT));
    }
    
    private static final String TRACKING_ID = "UA-48015716-1";
    private static final String CTIC_TRACKING_ID = "UA-48015717-1";
    private static final String PAGEVIEW = "pageview";
    private static final String EXPECTED_SNIPPET = 
        "  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
        "  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
        "  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
        "  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
        "  ga('create', '" + TRACKING_ID + "', 'auto');\n";
    private static final String EXPECTED_SNIPPET_CTIC = 
       EXPECTED_SNIPPET + 
       "ga('create', '" + CTIC_TRACKING_ID + "', 'auto', {'name': 'cticTracker'});\n";
    private static final String EXPECTED_SNIPPET_SEND = 
       EXPECTED_SNIPPET + 
       "ga('send', 'pageview');\n";
    private static final String EXPECTED_SNIPPET_EVENT = 
       EXPECTED_SNIPPET + 
       "ga('send', 'event', 'button', 'click');\n";
    private static final String JS_TEMPLATE = 
        "  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" +
        "  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" +
        "  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" +
        "  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n" +
        "  ga('create', 'UA-XXXX-Y', 'auto');";
    private static final String CTIC_LINE_TEMPLATE = 
        "ga('create', 'CTIC-UA-XXXX-Y', 'auto', {'name': 'cticTracker'});";
    private static final String SEND_LINE_TEMPLATE = 
        "ga('send', 'hit');";
     private static final String EVENT_LINE_TEMPLATE = 
        "ga('send', 'event', 'category', 'action');";
      private static final String EVENT_LINE_JQUERY_TEMPLATE = 
        "// Using jQuery Event API v1.3\n" +
        "$('jqueryid').on('action', function() {\n" +
        "  ga('send', 'event', 'category', 'action', 'label', value);\n" +
        "});";
    private static final String TRACKING_ID_REGEX = "UA-XXXX-Y";
    private static final String CTIC_TRACKING_ID_REGEX = "CTIC-UA-XXXX-Y";
    private static final String HIT_REGEX = "hit";
    private static final String CATEGORY_REGEX = "category";
    private static final String ACTION_REGEX = "action";
    private static final String CATEGORY = "button";
    private static final String ACTION = "click";
    private static final String LABEL_REGEX = "label";
    private static final String VALUE_REGEX = "value";
    private static final String JQUERY_ID_REGEX = "jqueryid";
    private JavaScriptWriter javaScriptWriter;
    private GoogleAnalyticsTracker googleAnalyticsTracker = 
            new GoogleAnalyticsTracker(TRACKING_ID);

}
