/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 12, 2014
 */

package org.fundacionctic.merkur.analytics.google;

/**
 * Writes the Google Analytics JS code snippet.
 * 
 * It takes a code template and replaces on it the contents of a 
 * given GoogleAnalyticsTracker instance.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class JavaScriptWriter {
    
    private GoogleAnalyticsConfiguration googleAnalyticsConfig;
    private JavaScriptEventWriter eventWriterHelper;
    
    /**
     * Crates a JavaScriptWriter instance.
     * 
     * @param googleAnalyticsConfig The Google Analytics configuration;
     * must be not null.
     * 
     * @throws IllegalArgumentException If the given googleAnalyticsConfig 
     * is null.
     */
    
    public JavaScriptWriter(GoogleAnalyticsConfiguration googleAnalyticsConfig) {
        if (googleAnalyticsConfig == null)
            throw new IllegalArgumentException("Trying to create a JavaScriptWriter "
                    + "with a null GoogleAnalyticsConfiguration. It must be not null");
        
        this.googleAnalyticsConfig = googleAnalyticsConfig;
        this.eventWriterHelper = new JavaScriptEventWriter(googleAnalyticsConfig);
    }

    /**
     * Writes the Google Analytics JS snippet.
     * 
     * @param googleAnalyticsTracker A GoogleAnalyticsTracker instance that allows
     * the generated Google Analytics JS snippet customization; must be not null.
     * @return A String containing the Google Analytics JS snippet.
     * 
     * @throws IllegalArgumentException If the given googleAnalyticsTracker is null.
     */
    public String writeSnippetFor(GoogleAnalyticsTracker googleAnalyticsTracker) {
        if (googleAnalyticsTracker == null)
            throw new IllegalArgumentException("Trying to write a "
                    + "Google Analytics JS snippet with a null googleAnalyticsTracker. "
                    + "The googleAnalyticsTracker must be not null");
        
        StringBuilder jsSnippetMainBlock = writeMainJSblock(googleAnalyticsTracker);
        
        if (isCticTrackerRequested(googleAnalyticsTracker)) 
            addCticTrakerLine(jsSnippetMainBlock, googleAnalyticsTracker);
        
        for (String each : googleAnalyticsTracker.getHits()) 
            addHitLine(jsSnippetMainBlock, each);
        
        addEventLines(jsSnippetMainBlock, googleAnalyticsTracker);
        
        return jsSnippetMainBlock.toString();
    }
    
    private StringBuilder writeMainJSblock(GoogleAnalyticsTracker googleAnalyticsTracker) {
        StringBuilder jsSnippetBuilder = new StringBuilder(
                this.googleAnalyticsConfig.getGoogleAnalyticsJSTemplate()
                .replace(googleAnalyticsConfig.getGoogleAnalyticsTrakingIdRegex(), 
                googleAnalyticsTracker.getTrackingId()));
        
        jsSnippetBuilder.append("\n");
        return jsSnippetBuilder;
    }
    
    private boolean isCticTrackerRequested(GoogleAnalyticsTracker googleAnalyticsTracker) {
        return googleAnalyticsTracker.getCticTrackingId() != null && 
                !googleAnalyticsTracker.getCticTrackingId().isEmpty();
    }
    
    private StringBuilder addCticTrakerLine(StringBuilder jsSnippetCode, GoogleAnalyticsTracker googleAnalyticsTracker) {
        jsSnippetCode.append(
                    googleAnalyticsConfig.getCticTrakerLineTemplate()
                    .replace(googleAnalyticsConfig.getCticTrakingIdRegex(), 
                    googleAnalyticsTracker.getCticTrackingId()));
        jsSnippetCode.append("\n");
        return jsSnippetCode;
    }
    
    private StringBuilder addHitLine(StringBuilder jsSnippetCode, String hit) {
        jsSnippetCode.append( 
                    googleAnalyticsConfig.getSendLineTemplate()
                    .replace(googleAnalyticsConfig.getHitRegex(), hit));
        jsSnippetCode.append("\n");
        return jsSnippetCode;
    }
    
    private StringBuilder addEventLines(StringBuilder jsSnippetCode, GoogleAnalyticsTracker googleAnalyticsTracker) {
        jsSnippetCode.append(
                eventWriterHelper.writeSnippetFor(googleAnalyticsTracker));
        return jsSnippetCode;
    }

}
