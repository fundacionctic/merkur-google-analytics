/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 12, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a Google Analytics traker object.
 * 
 * The tracker object is JS object used within the Google Analytics
 * JS snippet to identify the application sending the data and the
 * type of data that is going to be sent to the Google Analytics server.
 * 
 * The trackingID is a unique identifier that identifies the application
 * sending the data.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsTracker {
    
    private String trackingId;
    private String cticTrackingId;
    private List<String> hits = new ArrayList<String>();
    private Set<GoogleAnalyticsEvent> events = new HashSet<GoogleAnalyticsEvent>();

    /**
     * Creates a GoogleAnalyticsTracker instance.
     * 
     * @param trackingId Identifies the application that is going to send
     * data to the Google Analytics server; must be neither null not empty.
     * 
     * @throws IllegalArgumentException If the given trackingId is null or empty.
     */
    public GoogleAnalyticsTracker(String trackingId) {
        if (trackingId == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsTracker instance with a null trackingId. "
                    + "The trakingId must be neither null not empty");
        if (trackingId.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsTracker instance with an empty trackingId. "
                    + "The trakingId must be neither null not empty");
        
        this.trackingId = trackingId;
    }

    /**
     * Returns the Google Analytics application unique identifier.
     * 
     * @return The unique ID that Google uses to identify the application sending
     * data to the Analytics server.
     */
    public String getTrackingId() {
        return trackingId;
    }

    public String getCticTrackingId() {
        return cticTrackingId;
    }

    public void setCticTrackingId(String cticTrackingId) {
        this.cticTrackingId = cticTrackingId;
    }

    public void addHit(String hit) {
        hits.add(hit);
    }
    
    public void addEvent(GoogleAnalyticsEvent event) {
        events.add(event);
    }

    public List<String> getHits() {
        return hits;
    }

    public Set<GoogleAnalyticsEvent> getEvents() {
        return events;
    }  

}
