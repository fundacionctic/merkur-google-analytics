/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 12, 2014
 */

package org.fundacionctic.merkur.analytics.google;

/**
 * This class represents the configuration needed by the 
 * google-analytics-api to create the Google Analytics JS 
 * snippet code.
 * 
 * This configuration includes:
 * 
 * -The JS template code used as base to write the final snippet.
 *  This piece of code contains a basic Google Analytics snippet that
 *  will be customized and extended to add more features for a given
 *  application.
 * 
 * - The traking ID regex. The JS template includes a regex to identify
 *  the location within the snippet of the application unique identifier 
 *  (tracking ID). This regex will be replaced during the final snippet generation
 *  using the application unique identifier.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsConfiguration {
    
    private String jsTemplate; 
    private String trakingIdRegex;
    private String cticTrakerLineTemplate; 
    private String cticTrakingIdRegex;
    private String sendLineTemplate; 
    private String hitRegex;
    private String eventLineTemplate;
    private String jQueryEventLineTemplate;
    private String scrollEventLineTemplate;
    private String categoryRegex;
    private String actionRegex;
    private String labelRegex;
    private String valueRegex;
    private String jQueryIdRegex;

    /**
     * Creates a GoogleAnalyticsConfiguration instance.
     * 
     * @param jsTemplate The Google Analytics JS snippet template; 
     * must be neither null nor empty.
     * @param trakingIdRegex The regex used within the JS snippet 
     * template to identify the location of the application unique ID; 
     * must be neither null nor empty.
     * 
     * @throws IllegalArgumentException If one or both of the given jsTemplate 
     * and trakingIdRegex are null or empty.
     */
    GoogleAnalyticsConfiguration(String jsTemplate, String trakingIdRegex) {
        if (jsTemplate == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsConfiguration instance with a null jsTemplate. "
                    + "The jsTemplate must be neither null not empty");
        if (jsTemplate.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsConfiguration instance with an empty jsTemplate. "
                    + "The jsTemplate must be neither null not empty");
        if (trakingIdRegex == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsConfiguration instance with a null trakingIdRegex. "
                    + "The trakingIdRegex must be neither null not empty");
        if (trakingIdRegex.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsConfiguration instance with an empty trakingIdRegex. "
                    + "The trakingIdRegex must be neither null not empty");
        
        this.jsTemplate = jsTemplate;
        this.trakingIdRegex = trakingIdRegex;
    }
    
    /**
     * Returns the JS snippet template code.
     * 
     * @return A String containing the JS snippet template code.
     */
    public String getGoogleAnalyticsJSTemplate() {
        return jsTemplate;
    }
    
    /**
     * Returns the tracking ID regex.
     * 
     * @return A String containing the tracking ID regex value.
     */
    public String getGoogleAnalyticsTrakingIdRegex() {
        return trakingIdRegex;
    }

    public String getCticTrakerLineTemplate() {
        return cticTrakerLineTemplate;
    }

    public void setCticTrakerLineTemplate(String cticTrakerLineTemplate) {
        this.cticTrakerLineTemplate = cticTrakerLineTemplate;
    }

    public String getCticTrakingIdRegex() {
        return cticTrakingIdRegex;
    }

    public void setCticTrakingIdRegex(String cticTrakingIdRegex) {
        this.cticTrakingIdRegex = cticTrakingIdRegex;
    }

    public String getSendLineTemplate() {
        return sendLineTemplate;
    }

    public void setSendLineTemplate(String sendLineTemplate) {
        this.sendLineTemplate = sendLineTemplate;
    }

    public String getHitRegex() {
        return hitRegex;
    }

    public void setHitRegex(String hitRegex) {
        this.hitRegex = hitRegex;
    }

    public String getEventLineTemplate() {
        return eventLineTemplate;
    }

    public void setEventLineTemplate(String eventLineTemplate) {
        this.eventLineTemplate = eventLineTemplate;
    }

    public String getCategoryRegex() {
        return categoryRegex;
    }

    public void setCategoryRegex(String categoryRegex) {
        this.categoryRegex = categoryRegex;
    }

    public String getActionRegex() {
        return actionRegex;
    }

    public void setActionRegex(String actionRegex) {
        this.actionRegex = actionRegex;
    }

    public String getLabelRegex() {
        return labelRegex;
    }

    public void setLabelRegex(String labelRegex) {
        this.labelRegex = labelRegex;
    }

    public String getValueRegex() {
        return valueRegex;
    }

    public void setValueRegex(String valueRegex) {
        this.valueRegex = valueRegex;
    }

    public String getjQueryIdRegex() {
        return jQueryIdRegex;
    }

    public void setjQueryIdRegex(String jQueryIdRegex) {
        this.jQueryIdRegex = jQueryIdRegex;
    }

    public String getjQueryEventLineTemplate() {
        return jQueryEventLineTemplate;
    }

    public void setjQueryEventLineTemplate(String jQueryEventLineTemplate) {
        this.jQueryEventLineTemplate = jQueryEventLineTemplate;
    }

    public String getScrollEventLineTemplate() {
        return scrollEventLineTemplate;
    }

    public void setScrollEventLineTemplate(String scrollEventLineTemplate) {
        this.scrollEventLineTemplate = scrollEventLineTemplate;
    }

}
