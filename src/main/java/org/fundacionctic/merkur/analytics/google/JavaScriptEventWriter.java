/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 18, 2014
 */

package org.fundacionctic.merkur.analytics.google;

/**
 * Writes the JS Google Analytics event line.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class JavaScriptEventWriter {
    
    private GoogleAnalyticsConfiguration googleAnalyticsConfig;

    /**
     * Creates a JavaScriptEventWriter instance.
     * 
     * @param googleAnalyticsConfig The Google Analytics configuration;
     * must be not null.
     * 
     * @throws IllegalArgumentException If the given googleAnalyticsConfig 
     * is null.
     */
    public JavaScriptEventWriter(GoogleAnalyticsConfiguration googleAnalyticsConfig) {
        if (googleAnalyticsConfig == null)
            throw new IllegalArgumentException("Trying to create a JavaScriptEventWriter "
                    + "with a null GoogleAnalyticsConfiguration. It must be not null");
        
        this.googleAnalyticsConfig = googleAnalyticsConfig;
    }
    
    /**
     * Writes the Google Analytics JS EVENT snippet.
     * 
     * @param googleAnalyticsTracker A GoogleAnalyticsTracker instance that allows
     * the generated Google Analytics JS event snippet customization; must be not null.
     * @return A String containing the Google Analytics JS event snippet.
     * 
     * @throws IllegalArgumentException If the given googleAnalyticsTracker is null.
     */
    public String writeSnippetFor(GoogleAnalyticsTracker googleAnalyticsTracker) {
        if (googleAnalyticsTracker == null)
            throw new IllegalArgumentException("Trying to write a "
                    + "Google Analytics JS snippet with a null googleAnalyticsTracker. "
                    + "The googleAnalyticsTracker must be not null");
        
        StringBuilder jsSnippetBuilder = new StringBuilder();
        for (GoogleAnalyticsEvent each : googleAnalyticsTracker.getEvents()) {
            String jsEventTemplate = getJSEventCodeTemplate(each);
            if (isEventValueEmpty(each))
                jsEventTemplate = removeValueCodeFrom(jsEventTemplate);
            if (isEventLabelEmpty(each))
                jsEventTemplate = removeLabelCodeFrom(jsEventTemplate);
            
            replaceRegexWithValues(jsSnippetBuilder, jsEventTemplate, each);
        }
        return jsSnippetBuilder.toString();
    }

    private String getJSEventCodeTemplate(GoogleAnalyticsEvent event) {
        if (isJqueryEvent(event))
            return this.googleAnalyticsConfig.getjQueryEventLineTemplate();
        else
            if (isScrollEvent(event))
                return this.googleAnalyticsConfig.getScrollEventLineTemplate();
            else
                return this.googleAnalyticsConfig.getEventLineTemplate();
    }
    
    private boolean isJqueryEvent(GoogleAnalyticsEvent event) {
        return !event.getJqueryId().isEmpty();
    }
    
    private boolean isScrollEvent(GoogleAnalyticsEvent event) {
        return "scroll".equalsIgnoreCase(event.getAction());
    }
    
    private boolean isEventValueEmpty(GoogleAnalyticsEvent event) {
        return event.getValue().isEmpty();
    }
    
    private boolean isEventLabelEmpty(GoogleAnalyticsEvent event) {
        return event.getLabel().isEmpty();
    }
    
    private String removeValueCodeFrom(String code) {
        return code.replace(", value", "");
    }
    
    private String removeLabelCodeFrom(String code) {
        return code.replace(", 'label'", "");
    }
    
    private StringBuilder replaceRegexWithValues(StringBuilder jsSnippetBuilder, String jsEventTemplate, GoogleAnalyticsEvent event) {
        jsSnippetBuilder.append(
                jsEventTemplate
                .replace(googleAnalyticsConfig.getjQueryIdRegex(), 
                event.getJqueryId())
                .replace(googleAnalyticsConfig.getActionRegex(), 
                event.getAction())
                .replace(googleAnalyticsConfig.getCategoryRegex(), 
                event.getCategory())
                .replace(googleAnalyticsConfig.getLabelRegex(), 
                event.getLabel())
                .replace(googleAnalyticsConfig.getValueRegex(), 
                event.getValue()));
            jsSnippetBuilder.append("\n");
            return jsSnippetBuilder;
    }
}
