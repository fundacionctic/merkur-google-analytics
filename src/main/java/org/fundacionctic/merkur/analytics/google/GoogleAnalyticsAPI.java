/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 13, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class creates a very simple interface to create Google Analytics 
 * JS snippets.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsAPI {
    
    private static Logger log = LoggerFactory.getLogger(GoogleAnalyticsAPI.class);
    
    private JavaScriptWriter javaScriptWriter;
    private GoogleAnalyticsTracker googleAnalyticsTracker;
    
     /**
     * Creates a GoogleAnalyticsAPI instance.
     * 
     * @param googleAnalyticsConfigFilePath The Google Analytics configuration 
     * file path; must be neither null nor empty.
     * @param trackingId Identifies the application that is going to send
     * data to the Google Analytics server; must be neither null not empty.
     * 
     * @throws IllegalArgumentException If the googleAnalyticsConfigFilePath or
     * the trackingId is null or empty.
     */
    public static GoogleAnalyticsAPI newInstance(String googleAnalyticsConfigFilePath, String trackingId) {
        return new GoogleAnalyticsAPI(googleAnalyticsConfigFilePath, trackingId);
    }
    
    private GoogleAnalyticsAPI(String googleAnalyticsConfigFilePath, String trackingId) {
        if (googleAnalyticsConfigFilePath == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsAPI instance with a null googleAnalyticsConfigFilePath. "
                    + "The googleAnalyticsConfigFilePath must be neither null not empty");
        if (googleAnalyticsConfigFilePath.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsAPI instance with an empty googleAnalyticsConfigFilePath. "
                    + "The googleAnalyticsConfigFilePath must be neither null not empty");
        if (trackingId == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsAPI instance with a null trackingId. "
                    + "The trackingId must be neither null not empty");
        if (trackingId.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsAPI instance with an empty trackingId. "
                    + "The trackingId must be neither null not empty");
        
        log.debug("We are going to generate a Google Analytics JS "
                + "snippet with a trackingID:" + trackingId + " using the Google"
                + " Analytics configuration contained in: " + googleAnalyticsConfigFilePath);
        this.javaScriptWriter = new JavaScriptWriter(readConfiguration(googleAnalyticsConfigFilePath));
        this.googleAnalyticsTracker = new GoogleAnalyticsTracker(trackingId);
    }
    
    public GoogleAnalyticsAPI cticTrakingId(String cticTrackingId) {
        log.debug("The Google Analytics JS "
                + "snippet will have a CTIC trackingID:" + cticTrackingId);
        googleAnalyticsTracker.setCticTrackingId(cticTrackingId);
        return this;
    }
    
    public GoogleAnalyticsAPI send(String hit) {
        log.debug("The Google Analytics JS "
                + "snippet will have a send command with a hit:" + hit);
        googleAnalyticsTracker.addHit(hit);
        return this;
    }
    
    public GoogleAnalyticsAPI event(GoogleAnalyticsEvent event) {
        log.debug("The Google Analytics JS will have an :" + event);
        googleAnalyticsTracker.addEvent(event);
        return this;
    }

    public String generateJS() {
        String googleAnalyticsSnippet = this.javaScriptWriter
                 .writeSnippetFor(googleAnalyticsTracker);
        log.debug("Google Analytics JS snippet: \n" + googleAnalyticsSnippet);
        
        return googleAnalyticsSnippet;
    }
    
    private GoogleAnalyticsConfiguration readConfiguration(String googleAnalyticsConfigFilePath) {
        File configurationFile = new File(googleAnalyticsConfigFilePath);
        log.debug("Reading Google Analytics configuration from: " + configurationFile.getAbsolutePath());
         try {
            return new GoogleAnalyticsConfigurationReader().readFrom(configurationFile);
        } catch (FileNotFoundException ex) {
             log.error("The file: " + configurationFile.getAbsolutePath() + " was not found", ex);
             throw new RuntimeException("The file: " + configurationFile.getAbsolutePath() + " was not found", ex);
        } catch (IOException ex) {
             log.error("Couldn't read the file: " + configurationFile.getAbsolutePath(), ex);
             throw new RuntimeException("Couldn't read the file: " + configurationFile.getAbsolutePath(), ex);
        }
    }

}
