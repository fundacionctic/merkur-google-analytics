/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 18, 2014
 */

package org.fundacionctic.merkur.analytics.google;

/**
 * Represents an event within the Google Analytics API.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsEvent {
    
    private String category = ""; 
    private String action = "";
    private String label = "";
    private String value = "";
    private String jqueryId = "";

    /**
     * Creates a GoogleAnalyticsEvent new instance.
     * 
     * @param category The event category; must be neither null nor empty.
     * @param action The event category; must be neither null nor empty.
     * @return A new instance of a GoogleAnalyticsEvent object.
     * 
     * @throws IllegalArgumentException If one of the provided category or action
     * are null or empty.
     */
    public static GoogleAnalyticsEvent newInstance(String category, String action) {
        if (category == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with a null category. "
                    + "The category must be neither null not empty");
        if (category.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with an empty category. "
                    + "The category must be neither null not empty");
        if (action == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with a null action. "
                    + "The action must be neither null not empty");
        if (action.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with an empty action. "
                    + "The action must be neither null not empty");
        
        return new GoogleAnalyticsEvent(category, action);
    }
    
    private GoogleAnalyticsEvent(String category, String action) {
        this.category = category;
        this.action = action;
    }

    public GoogleAnalyticsEvent label(String label) {
        if (label == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with a null label. "
                    + "The label must be neither null not empty");
        if (label.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with an empty label. "
                    + "The label must be neither null not empty");
        
        this.label = label;
        return this;
    }
    
    public GoogleAnalyticsEvent value(String value) {
        if (value == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with a null value. "
                    + "The value must be neither null not empty");
        if (value.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with an empty value. "
                    + "The value must be neither null not empty");
        
        this.value = value;
        return this;
    }
    
    public GoogleAnalyticsEvent jqueryId(String jqueryId) {
        if (jqueryId == null)
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with a null jqueryId. "
                    + "The jqueryId must be neither null not empty");
        if (jqueryId.isEmpty())
            throw new IllegalArgumentException("Trying to create a "
                    + "GoogleAnalyticsEvent instance with an empty jqueryId. "
                    + "The jqueryId must be neither null not empty");
        
        this.jqueryId = jqueryId;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public String getJqueryId() {
        return jqueryId;
    }

    @Override
    public String toString() {
        return "Google Analytics Event with category: " + category + ", action: " + action + ", label: " + label + ", value: " + value + ", jqueryId: " + jqueryId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.category != null ? this.category.hashCode() : 0);
        hash = 47 * hash + (this.action != null ? this.action.hashCode() : 0);
        hash = 47 * hash + (this.label != null ? this.label.hashCode() : 0);
        hash = 47 * hash + (this.value != null ? this.value.hashCode() : 0);
        hash = 47 * hash + (this.jqueryId != null ? this.jqueryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GoogleAnalyticsEvent other = (GoogleAnalyticsEvent) obj;
        if ((this.category == null) ? (other.category != null) : !this.category.equals(other.category)) {
            return false;
        }
        if ((this.action == null) ? (other.action != null) : !this.action.equals(other.action)) {
            return false;
        }
        if ((this.label == null) ? (other.label != null) : !this.label.equals(other.label)) {
            return false;
        }
        if (this.value != other.value) {
            return false;
        }
        if ((this.jqueryId == null) ? (other.jqueryId != null) : !this.jqueryId.equals(other.jqueryId)) {
            return false;
        }
        return true;
    }

}
