/*
 * Fundación CTIC
 * http://www.fundacionctic.org
 * File created on: Feb 12, 2014
 */

package org.fundacionctic.merkur.analytics.google;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reads the configuration needed to create a Google Analytics JS snippet.
 * 
 * Reads the configuration from a provided properties file.
 * 
 * @author josemanuel
 * @version 1.0
 */
public class GoogleAnalyticsConfigurationReader {
    
    private static Logger log = LoggerFactory.getLogger(GoogleAnalyticsConfigurationReader.class);

    private static GoogleAnalyticsConfiguration configuration;
    /**
     * Reads the google-analytics-api configuration.
     * 
     * @param configurationPropertiesFile The properties file that contains the 
     * configuration; must be not null.
     * 
     * @return A GoogleAnalyticsConfiguration instance containing the 
     * google-analytics-api configuration.
     * 
     * @throws FileNotFoundException If the properties file was not found.
     * @throws IOException If there is a problem reading the properties file.
     * @throws IllegalArgumentException If the given configurationPropertiesFile is null.
     * @throws IllegalArgumentException If an expected property was not found within
     * the properties file.
     */
    public GoogleAnalyticsConfiguration readFrom(File configurationPropertiesFile) throws FileNotFoundException, IOException {
        if (configurationPropertiesFile == null)
            throw new IllegalArgumentException("Trying to read a "
                    + "Google Analytics configuration file with a null configurationPropertiesFile. "
                    + "The configurationPropertiesFile must be not null");
        
        if (configuration != null)
            return configuration;
        
        log.debug("Reading google analytics configuration file... ");
        Properties properties = new Properties();
        properties.load(new FileInputStream(configurationPropertiesFile));
        String trackIdRegex = getProperty(properties, GOOGLE_ANALYTICS_TRACK_ID_REGEX);
        String jsTemplateCode = getJSTemplateFileContent(getProperty(properties, GOOGLE_ANALYTICS_JS_TEMPLATE_PATH));
        log.debug("Configured JS snippet template code: " + jsTemplateCode);
        log.debug("Configured track ID regex: " + trackIdRegex);
        GoogleAnalyticsConfiguration result = 
                new GoogleAnalyticsConfiguration(jsTemplateCode,trackIdRegex);
        String cticTrackerTemplateCodeLine = getJSTemplateFileContent(getProperty(properties, GOOGLE_ANALYTICS_CTIC_LINE_PATH));
        log.debug("Configured ctic tracker template code line: " + cticTrackerTemplateCodeLine);
        result.setCticTrakerLineTemplate(cticTrackerTemplateCodeLine);
        String cticTrackerIdRegex = getProperty(properties, GOOGLE_ANALYTICS_CTIC_ID_REGEX);
        log.debug("Configured CTIC track ID regex: " + trackIdRegex);
        result.setCticTrakingIdRegex(cticTrackerIdRegex);
        String sendTemplateCodeLine = getJSTemplateFileContent(getProperty(properties, GOOGLE_ANALYTICS_SEND_LINE_PATH));
        log.debug("Configured send template code line: " + sendTemplateCodeLine);
        result.setSendLineTemplate(sendTemplateCodeLine);
        String hitRegex = getProperty(properties, GOOGLE_ANALYTICS_HIT_REGEX);
        log.debug("Configured hit regex: " + hitRegex);
        result.setHitRegex(hitRegex);
        String eventLineTemplate = getJSTemplateFileContent(getProperty(properties, GOOGLE_ANALYTICS_EVENT_LINE_PATH));
        log.debug("Configured event template code line: " + eventLineTemplate);
        result.setEventLineTemplate(eventLineTemplate);
        String scrollEventLineTemplate = getJSTemplateFileContent(getProperty(properties, GOOGLE_ANALYTICS_SCROLL_EVENT_LINE_PATH));
        log.debug("Configured scroll event template code line: " + scrollEventLineTemplate);
        result.setScrollEventLineTemplate(scrollEventLineTemplate);
        String jQueryEventLineTemplate = getJSTemplateFileContent(getProperty(properties, GOOGLE_ANALYTICS_JQUERY_EVENT_LINE_PATH));
        log.debug("Configured jQuery event template code line: " + jQueryEventLineTemplate);
        result.setjQueryEventLineTemplate(jQueryEventLineTemplate);
        String categoryRegex = getProperty(properties, GOOGLE_ANALYTICS_CATEGORY_REGEX);
        log.debug("Configured category regex: " + categoryRegex);
        result.setCategoryRegex(categoryRegex);
        String actionRegex = getProperty(properties, GOOGLE_ANALYTICS_ACTION_REGEX);
        log.debug("Configured action regex: " + actionRegex);
        result.setActionRegex(actionRegex);
        String labelRegex = getProperty(properties, GOOGLE_ANALYTICS_LABEL_REGEX);
        log.debug("Configured label regex: " + labelRegex);
        result.setLabelRegex(labelRegex);
        String valueRegex = getProperty(properties, GOOGLE_ANALYTICS_VALUE_REGEX);
        log.debug("Configured value regex: " + valueRegex);
        result.setValueRegex(valueRegex);
        String jQueryIdRegex = getProperty(properties, GOOGLE_ANALYTICS_JQUERY_ID_REGEX);
        log.debug("Configured jQueryId regex: " + jQueryIdRegex);
        result.setjQueryIdRegex(jQueryIdRegex);
        configuration = result;
        return configuration;
    }
    
    private String getProperty(Properties properties, String propertyKey) {
        String propertyValue = properties.getProperty(propertyKey);
        if (propertyValue == null)
            throw new IllegalArgumentException("The Google Analytics properties file "
                    + "does not contain a value for the key: " + propertyKey + 
                    ". A value for that key was expected");
        
        return propertyValue;
    }
    
    private String getJSTemplateFileContent(String jsTemplateFilePath) throws IOException {
        return getJSTemplateFileContent(new File(jsTemplateFilePath));
    }
    
    private String getJSTemplateFileContent(File jsTemplateFile) throws IOException {
        if (!jsTemplateFile.exists())
            throw new IllegalArgumentException("The property "
                    + "javaScript.snippet.template.path within the google analytics "
                    + "configuration file seems to be pointing to a file that does not exist: "
                    + jsTemplateFile.getAbsolutePath());
        
        return FileUtils.readFileToString(jsTemplateFile);
    }
    
    private static final String GOOGLE_ANALYTICS_JS_TEMPLATE_PATH =
            "javaScript.snippet.template.path";
    private static final String GOOGLE_ANALYTICS_TRACK_ID_REGEX =
            "javaScript.snippet.trackid.regex";
     private static final String GOOGLE_ANALYTICS_CTIC_LINE_PATH =
            "javaScript.snippet.ctictrackid.path";
    private static final String GOOGLE_ANALYTICS_CTIC_ID_REGEX =
            "javaScript.snippet.ctictrackid.regex";
    private static final String GOOGLE_ANALYTICS_SEND_LINE_PATH =
            "javaScript.snippet.send.path";
    private static final String GOOGLE_ANALYTICS_HIT_REGEX =
            "javaScript.snippet.hit.regex";
    private static final String GOOGLE_ANALYTICS_EVENT_LINE_PATH =
            "javaScript.snippet.event.path";
    private static final String GOOGLE_ANALYTICS_CATEGORY_REGEX =
            "javaScript.snippet.category.regex";
    private static final String GOOGLE_ANALYTICS_ACTION_REGEX =
            "javaScript.snippet.action.regex";
    private static final String GOOGLE_ANALYTICS_LABEL_REGEX =
            "javaScript.snippet.label.regex";
    private static final String GOOGLE_ANALYTICS_VALUE_REGEX =
            "javaScript.snippet.value.regex";
    private static final String GOOGLE_ANALYTICS_JQUERY_ID_REGEX =
            "javaScript.snippet.jqueryid.regex";
    private static final String GOOGLE_ANALYTICS_JQUERY_EVENT_LINE_PATH =
            "javaScript.snippet.event.jquery.path";
    private static final String GOOGLE_ANALYTICS_SCROLL_EVENT_LINE_PATH =
            "javaScript.snippet.event.scroll.path";

}
